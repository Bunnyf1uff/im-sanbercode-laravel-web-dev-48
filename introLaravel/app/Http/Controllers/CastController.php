<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    // Create
    public function create() {
        return view("cast-templates.create");
    }
    
    public function store(Request $request) {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);
        
        DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);
        return redirect('/cast');
    }
    
    // Read
    public function index() {
        $cast = DB::table('cast')->get();
 
        return view('cast-templates.index', ['cast' => $cast]);
    }
    
    public function show($id) {
        $castData = DB::table('cast')->find($id);

        return view('cast-templates.data', ['castData' => $castData]);
    }

    // Update
    public function edit($id) {
        $castData = DB::table('cast')->find($id);

        return view('cast-templates.edit', ['castData' => $castData]);
    }

    public function update($id, Request $request) {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        DB::table('cast')
              ->where('id', $id)
              ->update([
                'nama' => $request['nama'],
                'umur' => $request['umur'],
                'bio' => $request['bio'],

            ]);
            return redirect('/cast');
    }

    // Delete
    public function destroy($id) {
        DB::table('cast')->where('id', '=', $id)->delete();
        
        return redirect('/cast');
    }
}
