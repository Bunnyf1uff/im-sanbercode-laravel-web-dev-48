@extends('layouts.master')
@section('title', 'Register')

@section('content')
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf
        <label>First Name:</label> <br><br>
        <input type="text" name="fname"> <br><br>
        <label>Last Name:</label> <br><br>
        <input type="text" name="lname"> <br><br>
        <label>Gender:</label> <br><br>
        <input type="radio" name="gender" value="1">Male <br>
        <input type="radio" name="gender" value="2">Female <br>
        <input type="radio" name="gender" value="3">Other <br><br>
        <label>Nationality:</label> <br><br>
        <select name="Nationality">
            <option value="1" name="nationality">Indonesian</option>
            <option value="2" name="nationality">Singapore</option>
            <option value="3" name="nationality">Malaysian</option>
            <option value="4" name="nationality">Australian</option>
        </select> <br> <br>
        <label>Language Spoken:</label> <br><br>
        <input type="checkbox" value="1" name="spoken">Bahasa Indonesia<br>
        <input type="checkbox" value="2" name="spoken">English<br>
        <input type="checkbox" value="3" name="spoken">Other<br><br>
        <label>Bio:</label> <br><br>
        <textarea cols="30" rows="10"></textarea><br>
        <button type="submit">Sign Up</button>
    </form>
@endsection
