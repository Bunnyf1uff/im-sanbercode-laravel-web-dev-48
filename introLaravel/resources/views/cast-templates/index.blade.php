@extends('layouts.master')

@section('title', 'Cast')

@section('content')
    <a href="/cast/create" class="btn btn-secondary my-3">Add Cast</a>
    <table class="table">
        <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Umur</th>
                <th scope="col">Aksi</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($cast as $key=> $item)
                <tr>
                    <th scope="row">{{$key + 1}}</th>
                    <td>{{$item->nama}}</td>
                    <td>{{$item->umur}}</td>
                    <td class="d-flex">
                        <a href="/cast/{{$item->id}}"><button class="btn btn-dark btn-sm mr-2">detail</button></a>
                        <a href="/cast/{{$item->id}}/edit"><button class="btn btn-warning btn-sm mr-2">edit</button></a>
                        <form action="/cast/{{$item->id}}" method="post">
                            @csrf
                            @method('delete')
                            <button type="submit" class="btn btn-danger btn-sm">delete</button>
                        </form>
                    </td>
                </tr>
            @empty
            <tr>
                <td>Data kosong</td>
            </tr>
            @endforelse
        </tbody>
    </table>

@endsection
