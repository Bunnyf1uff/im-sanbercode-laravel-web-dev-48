@extends('layouts.master')

@section('title', 'Edit Cast')

@section('content')
    <form action="/cast/{{$castData->id}}" method="post">
        @csrf
        @method('put')
        <div class="form-group">
            <label>Nama</label>
            <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" value="{{$castData->nama}}">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Umur</label>
            <input type="number" class="form-control @error('umur') is-invalid @enderror" name="umur" value="{{$castData->umur}}">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Bio</label>
            <textarea class="form-control @error('bio') is-invalid @enderror" name="bio" cols="30" rows="5">{{$castData->bio}}</textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
