@extends('layouts.master')

@section('title', 'Detail Cast')

@section('content')
    <h1>{{$castData->nama}}, {{$castData->umur}} tahun</h1>
    <p>{{$castData->bio}}</p>
@endsection