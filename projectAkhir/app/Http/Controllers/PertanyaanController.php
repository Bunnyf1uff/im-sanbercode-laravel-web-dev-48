<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Pertanyaan;
use Auth;
use File;

class PertanyaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pertanyaan = Pertanyaan::all();
        return view('pertanyaan-temp.index', ['pertanyaan' => $pertanyaan]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::all();
        return view('pertanyaan-temp.create', ['category'=>$category]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'gambar' => 'required|mimes:jpg,jpeg,png|max:2048',
            'content' => 'required',
            'category_id' => 'required',
        ]);

        $gambarName = time().'.'.$request->gambar->extension();  
   
        $request->gambar->move(public_path('gambar'), $gambarName);

        $pertanyaan = new Pertanyaan;
 
        $pertanyaan->content = $request['content'];
        $pertanyaan->gambar = $gambarName;
        $pertanyaan->category_id = $request['category_id'];
        $pertanyaan->user_id = Auth::id();

        $pertanyaan->save();
 
        return redirect('/pertanyaan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pertanyaan = Pertanyaan::find($id);
        return view('pertanyaan-temp.detail', ['pertanyaan'=> $pertanyaan]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pertanyaan = Pertanyaan::find($id);
        $category = Category::all();

        if (Auth::id() === $pertanyaan->user_id) {
            return view('pertanyaan-temp.edit', ['pertanyaan'=>$pertanyaan, 'category'=> $category]);
        } else {
            return redirect('/error');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'gambar' => 'mimes:jpg,jpeg,png|max:2048',
            'content' => 'required',
            'category_id' => 'required',
        ]);

        $pertanyaan = Pertanyaan::find($id);

        if($request->has('gambar')) {
            $path = 'gambar/';
            File::delete($path. $pertanyaan->gambar);

            $gambarName = time().'.'.$request->gambar->extension();  
   
            $request->gambar->move(public_path('gambar'), $gambarName);

            $pertanyaan->gambar = $gambarName;
        }
        $pertanyaan->content = $request['content'];
        $pertanyaan->category_id = $request['category_id'];

        $pertanyaan->save();

        return redirect('/pertanyaan');
   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pertanyaan = Pertanyaan::find($id);

        $userId = $pertanyaan['user_id'];
        if (Auth::id() === $userId) {
            $path = 'gambar/';
            File::delete($path. $pertanyaan->gambar);
     
            $pertanyaan->delete();
    
            return redirect('/pertanyaan');
        } else {
            return redirect('/error');
        }
    }
}
