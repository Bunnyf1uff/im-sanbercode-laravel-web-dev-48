@extends('layouts.master')

@section('title', 'Ubah Profile')

@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('templates/vendors/mdi/css/materialdesignicons.min.css') }}"/>
@endpush

@section('content')
    <form action="/profile/{{$profile->id}}" method="post">
        @method('put')
        @csrf

        <div class="form-group">
            <label>Nama</label>
            <input type="text" name="umur" class="form-control" value="{{$profile->user->name}}" disabled>
        </div>
        <div class="form-group">
            <label>Email</label>
            <input type="text" name="email" class="form-control" value="{{$profile->user->email}}" disabled>
        </div>
        <div class="form-group">
            <label>Umur</label>
            <input type="number" name="umur" class="form-control @error('umur') is-invalid @enderror" value="{{$profile->umur}}">
            @error('umur')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label>Biografi</label>
            <textarea cols="30" rows="10" type="text" name="bio" class="form-control @error('bio') is-invalid @enderror" value="{{$profile->bio}}">{{$profile->bio}}</textarea>
            @error('bio')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label>Alamat</label>
            <input type="text" name="alamat" class="form-control @error('alamat') is-invalid @enderror" value="{{$profile->alamat}}">
            @error('alamat')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Ubah</button>
    </form>
@endsection
