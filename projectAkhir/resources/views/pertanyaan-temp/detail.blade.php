@extends('layouts.master')

@section('title', 'About Question')

@section('content')
    <div class="card">
        <div class="card-body">
            <h5 class="card-title text-info"><small class="text-secondary ml-2">{{ $pertanyaan->user->name }} |
                    {{ $pertanyaan->updated_at->format('l, d F Y ') }}</small></h5>

            <h3 class="card-text">{{ $pertanyaan->content }}</h3>
        </div>
        <img class="card-img-top rounded mx-auto img-fluid" src="{{ asset('gambar/' . $pertanyaan->gambar) }}"
            alt="Card image cap" style="max-width: 30%; height: 25%;">
    </div>

    <br>
    <br>
    <hr>
    <h4>List Jawaban</h4>
    @forelse ($pertanyaan->jawaban as $item)
        <div class="card">
            <div class="card-header">
                {{ $item->user->name }}
            </div>
            <div class="card-body">
                <h5 class="card-text">{{ $item->content }}</h5>
                <div class="d-flex">
                    <a href="/jawaban/{{ $item->id }}/edit" class="btn btn-warning btn-sm mt-5 mr-2">Edit</a>
                    <form action="/jawaban/{{ $item->id }}" method="post">
                        @csrf
                        @method('delete')
                        <button type="submit" class="btn btn-danger btn-sm mt-5">Delete</button>
                    </form>
                </div>
            </div>
        </div>
    @empty
        <h6 class="text-secondary">Tidak ada jawaban</h6>
    @endforelse
    <hr>
    <form action="/jawaban/{{ $pertanyaan->id }}" method="post">
        @csrf
        <div class="form-group">
            <textarea name="content" id="" cols="30" rows="10"
                class="form-control @error('content') is-invalid @enderror" placeholder="Isi Jawaban"></textarea>
            @error('content')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Add Answer</button>
    </form>
@endsection
