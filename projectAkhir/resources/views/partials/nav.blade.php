<nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
    <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
      <a class="navbar-brand brand-logo mr-5" href="/dashboard"><img src="{{asset('templates/images/logo.svg')}}" class="mr-2" alt="logo"/></a>
      <a class="navbar-brand brand-logo-mini" href="/dashboard"><img src="{{asset('templates/images/logo-mini.svg')}}" alt="logo"/></a>
    </div>
    <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
      <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
        <span class="icon-menu"></span>
      </button>
      <ul class="navbar-nav mr-lg-2">
        <li class="nav-item nav-search d-none d-lg-block">
          <div class="input-group">
            <div class="input-group-prepend hover-cursor" id="navbar-search-icon">
              <span class="input-group-text" id="search">
              </span>
            </div>
          </div>
        </li>
      </ul>
      <ul class="navbar-nav navbar-nav-right">
    </div>
  </nav>