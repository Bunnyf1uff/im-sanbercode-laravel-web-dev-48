@extends('layouts.master')

@section('title', 'Edit Answer')

@section('content')
    <form action="/jawaban/{{ $jawaban->id }}" method="post">
        @method('put')
        @csrf
        <div class="form-group">
            <textarea name="content" id="" cols="30" rows="10" class="form-control @error('content') is-invalid @enderror">{{$jawaban->content}}</textarea>
            @error('content')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
