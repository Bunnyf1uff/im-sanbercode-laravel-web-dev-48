@extends('layouts.master')

@section('title')
    Kategori {{ $category->nama }}
@endsection
@section('content')
    @forelse ($category->pertanyaan as $item)
        <div class="card">
            <div class="card-body">
                <h5 class="card-title text-info">{{ $item->user->name }} <small
                        class="text-secondary ml-2">{{ $item->updated_at }}</small></h5>
                <h5 class="card-text">{{ Str::limit($item->content, 100) }}</h5>
            </div>
            <img class="card-img-top rounded mx-auto img-fluid" src="{{ asset('gambar/' . $item->gambar) }}"
                alt="Card image cap" style="max-width: 30%; height: 25%;">
            <div class="card-body d-flex justify-content-center">
                <a href="/pertanyaan/{{ $item->id }}" class="btn btn-primary btn-sm mr-2">Detail</a>
            </div>
            <div class="border-bottom mt-5"></div>
        </div>
    @empty
        <h4 class="text-center">Tidak ada pertanyaan terkait</h4>
    @endforelse
@endsection
