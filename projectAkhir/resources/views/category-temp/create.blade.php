@extends('layouts.master')

@section('title', 'Add Category')

@section('content')
    <form action="/category" method="post">
        @csrf
        <div class="form-group">
            <label>Nama Category</label>
            <input type="text" name="nama" class="form-control @error('nama') is-invalid @enderror">
            @error('nama')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
