@extends('layouts.master')

@section('title', 'Edit Category')

@section('content')
    <form action="/category/{{$category->id}}" method="post">
        @method('put')
        @csrf
        <div class="form-group">
            <label>Nama Category</label>
            <input type="text" name="nama" class="form-control @error('nama') is-invalid @enderror" value="{{$category->nama}}">
            @error('nama')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
