@extends('layouts.master')

@section('title', 'Dashboard')

@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('templates/vendors/mdi/css/materialdesignicons.min.css') }}"/>
@endpush

@section('content')
    <div class="d-sm-flex justify-content-between align-items-start">
        <div>
            <h4 class="card-title card-title-dash">Selamat Datang di Insightify!</h4>
            <h5 class="card-subtitle card-subtitle-dash">Di sini, Anda dapat berpartisipasi dalam diskusi, bertanya, menjawab, dan berbagi wawasan dengan sesama.</h5>
        </div>
    </div>
    <div class="template-demo d-flex justify-content-around flex-nowrap">
        <a class="nav-link" href="/pertanyaan">
            <button type="button" class="btn btn-primary btn-icon-text">
                <i class="mdi mdi-comment-question-outline btn-icon-prepend"></i>
                Daftar Pertanyaan
            </button>
        </a>
        <a class="nav-link" href="/pertanyaan/create">
            <button type="button" class="btn btn-secondary btn-icon-text">
                <i class="mdi mdi-send btn-icon-prepend"></i>   
                Ajukan Pertanyaan Anda                       
            </button>
        </a>
        <a class="nav-link" href="/category/create">
            <button type="button" class="btn btn-success btn-icon-text">
                <i class="mdi mdi-view-headline btn-icon-prepend"></i>                                                    
                Buat Kategori Pertanyaan
            </button>
        </a>
    </div>
@endsection
